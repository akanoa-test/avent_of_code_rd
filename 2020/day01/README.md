# Day 01
Petits tests de performances sur un algo plus optimisés


Deux algos:

Un naïf:

```rust
fn brute_force(tab: &Vec<i32>) -> Option<u32> {
    for x in tab {
        for y in tab {
            for z in tab {
                if x + y + z == 2020 {
                    return Some((x * y * z) as u32)
                }
            }
        }
    }
    None
}
```

Un plus efficace qui tire parti de la fonction de hachage d'un set pour
éliminer efficacement les les entrés qui ne peuvent pas correspondre sans avoir à les lire
en mémoire.

On optimise aussi le fait que le triplet ne peut pas contenir deux fois le même 
nombre:

```rust
fn a_little_bit_more_optimized(tab: &mut Vec<i32>) -> Option<u32> {


    let hash_set = HashSet::<i32>::from_iter(tab.iter().copied());

    hash_set.iter().find_map(|x| { // On boucle sur les x
        hash_set.iter()
            .filter(|&y| x != y) // On teste pas les cas x = y
            .find_map(|y| { // On boucle sur (x, y)
                hash_set
                    .get(&(2020 - x - y)) // On cherche z = 2020 - x - y
                    .map(|z| (x * y * z) as u32) // On effectue le produit
            })
    })
}
```

## Résult du bench

Les tests bench sont réalisés grâce à `cargo bench` en mode blackbox.

```
test tests::bench_a_little_bit_more_optimized        ... bench:     215,746 ns/iter (+/- 40,007)
test tests::bench_brute_force                        ... bench:     935,363 ns/iter (+/- 77,340)
```

On voit que l'algorithme optimisé est 5 fois plus efficace.

## Explication du deuxième algorithme

Un hash set attribue à chaque entré de la collection un signature unique;

Lors de l'algorithme on effectue une double boucle à la fois sur x et sur y.

Pour des fins d'optimisations ( surtout lorsque le tableau est très grand), on élémine via le `filter` les duo où x == y.

Une fois ce duo (x, y) trouvé, on recherche z tel z = 2020 - x - y.

Si le hash set contient une clef dont la signature correspond à cette soustration. 
On retourne z. Puis on multiplie ce z avec les composantes x et y.

Sinon on passe au duo (x, y) suivant et on réitère l'opération, jusqu'à trouver un  z ou que x atteigne la fin de la collection.
