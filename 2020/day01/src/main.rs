#![feature(test)]
use std::collections::HashSet;
use std::iter::FromIterator;
extern crate test;

fn main() {

}

fn a_little_bit_more_optimized(tab: &mut Vec<i32>) -> Option<u32> {


    let hash_set = HashSet::<i32>::from_iter(tab.iter().copied());

    hash_set.iter().find_map(|x| {
        hash_set.iter()
            .filter(|&y| x != y)
            .find_map(|y| {
                hash_set
                    .get(&(2020 - x - y))
                    .map(|z| (x * y * z) as u32)
            })
    })
}

fn brute_force(tab: &Vec<i32>) -> Option<u32> {
    for x in tab {
        for y in tab {
            for z in tab {
                if x + y + z == 2020 {
                    return Some((x * y * z) as u32)
                }
            }
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use crate::{a_little_bit_more_optimized, brute_force};
    use std::fs::File;
    use std::io::{BufReader, BufRead};
    use test::Bencher;

    #[test]
    fn should_test_a_little_bit_more_optimized() {
        let mut input = vec![1721,
                             979,
                             366,
                             299,
                             675,
                             1456];
        let expected = Some(241861950);
        let result = a_little_bit_more_optimized(&mut input);
        assert_eq!(result, expected);
    }

    #[test]
    fn should_test_brute_forced() {
        let input = vec![1721,
                         979,
                         366,
                         299,
                         675,
                         1456];
        let expected = Some(241861950);
        let result = brute_force(&input);
        assert_eq!(result, expected);
    }

    #[bench]
    fn bench_a_little_bit_more_optimized(b : &mut Bencher) {

        let file = File::open("./src/input.txt")
            .expect("fail");

        let reader = BufReader::new(file);

        let mut numbers: Vec<i32> = reader
            .lines()
            .map(|x| x.unwrap().parse::<i32>().unwrap())
            .collect();

        b.iter(||{
            test::black_box(a_little_bit_more_optimized(&mut numbers));
        })
    }

    #[bench]
    fn bench_brute_force(b : &mut Bencher) {
        let file = File::open("./src/input.txt")
            .expect("fail");

        let reader = BufReader::new(file);

        let numbers : Vec<i32> = reader
            .lines()
            .map(|x| x.unwrap().parse::<i32>().unwrap())
            .collect();

        b.iter(||{
            test::black_box(brute_force(&numbers));
        })
    }
}
